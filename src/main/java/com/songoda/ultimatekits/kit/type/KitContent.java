package com.songoda.ultimatekits.kit.type;

import org.bukkit.inventory.ItemStack;

public interface KitContent {

    String getSerialized();

    ItemStack getItemForDisplay();

}
